
# extract point data

This ROS-Node can be used to detect the angle between walls in a room aswell as the position of the corner points.

Needs:
- ROS
- PCL

You can use it in combination with a linear laser, until now just 2D is supported.

Use the launchfile to select the number of Walls.

TBD:
- 3D
- Documentation
- Dynamic Reconfigure


<p align="center"><img src="pictures/measurement1.png" /></p>

<p align="center"><img src="pictures/measurement2.png" /></p>

<p align="center"><img src="pictures/measurement3.png" /></p>
